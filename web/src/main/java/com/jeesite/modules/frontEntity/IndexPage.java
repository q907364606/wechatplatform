package com.jeesite.modules.frontEntity;

import java.io.Serializable;
import java.util.List;

import com.jeesite.common.entity.Page;
import com.jeesite.modules.t_indexbanner.entity.Tindexbanner;
import com.jeesite.modules.t_information.entity.Tinformation;

public class IndexPage implements Serializable{

	private List<Tinformation> tinformationList; 
	
	private List<Tindexbanner> tindexbannerList;
	
	
	public IndexPage(List<Tinformation> tinformationList, List<Tindexbanner> tindexbannerList) {
		super();
		this.tinformationList = tinformationList;
		this.tindexbannerList = tindexbannerList;
	}
	
	
	public List<Tinformation> getTinformationList() {
		return tinformationList;
	}

	public void setTinformationList(List<Tinformation> tinformationList) {
		this.tinformationList = tinformationList;
	}

	
	
	public List<Tindexbanner> getTindexbannerList() {
		return tindexbannerList;
	}

	public void setTindexbannerList(List<Tindexbanner> tindexbannerList) {
		this.tindexbannerList = tindexbannerList;
	}

	
	
}
