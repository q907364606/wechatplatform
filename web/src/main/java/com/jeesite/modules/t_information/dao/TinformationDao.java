/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_information.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.t_information.entity.Tinformation;

/**
 * 系统资讯DAO接口
 * @author zwz
 * @version 2018-10-18
 */
@MyBatisDao
public interface TinformationDao extends CrudDao<Tinformation> {
	
	
	
}