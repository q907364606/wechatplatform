/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_information.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.t_information.entity.Tinformation;
import com.jeesite.modules.t_information.dao.TinformationDao;
import com.jeesite.modules.file.utils.FileUploadUtils;

/**
 * 系统资讯Service
 * @author zwz
 * @version 2018-10-18
 */
@Service
@Transactional(readOnly=true)
public class TinformationService extends CrudService<TinformationDao, Tinformation> {
	
	/**
	 * 获取单条数据
	 * @param tinformation
	 * @return
	 */
	@Override
	public Tinformation get(Tinformation tinformation) {
		return super.get(tinformation);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param tinformation
	 * @return
	 */
	@Override
	public Page<Tinformation> findPage(Page<Tinformation> page, Tinformation tinformation) {
		return super.findPage(page, tinformation);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param tinformation
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Tinformation tinformation) {
		super.save(tinformation);
		// 保存上传图片
		FileUploadUtils.saveFileUpload(tinformation.getId(), "tinformation_image");
		// 保存上传附件
		FileUploadUtils.saveFileUpload(tinformation.getId(), "tinformation_file");
	}
	
	/**
	 * 更新状态
	 * @param tinformation
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Tinformation tinformation) {
		super.updateStatus(tinformation);
	}
	
	/**
	 * 删除数据
	 * @param tinformation
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Tinformation tinformation) {
		super.delete(tinformation);
	}
	
}