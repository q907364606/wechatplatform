/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_information.entity;

import java.util.Date;

import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 系统资讯Entity
 * @author zwz
 * @version 2018-10-18
 */
@Table(name="t_information", alias="a", columns={
		@Column(name="infoid", attrName="infoid", label="主键id 这张表示系统中发送的资讯", isPK=true),
		@Column(name="infoname", attrName="infoname", label="推荐资讯名称"),
		@Column(name="department", attrName="department", label="资讯发送部门"),
		@Column(name="detailurl", attrName="detailurl", label="跳转路径"),
		@Column(name="img", attrName="img", label="图片0"),
		@Column(name="img1", attrName="img1", label="图片1"),
		@Column(name="img2", attrName="img2", label="图片2"),
		@Column(name="ico", attrName="ico", label="ico"),
		@Column(name="videourl", attrName="videourl", label="视频路径"),
		@Column(includeEntity=DataEntity.class),
		@Column(name="isrecommend", attrName="isrecommend", label="是否推荐 1表示推荐 0表示不推荐"),
		@Column(name="senderid", attrName="senderid", label="发送者id"),
		@Column(name="sendername", attrName="sendername", label="发送者姓名"),
		@Column(name="extend1", attrName="extend1", label="extend1"),
		@Column(name="extend2", attrName="extend2", label="extend2"),
		@Column(name="extend3", attrName="extend3", label="extend3"),
	}, orderBy="a.update_date DESC"
)
public class Tinformation extends DataEntity<Tinformation> {
	
	private static final long serialVersionUID = 1L;
	private String infoid;		// 主键id 这张表示系统中发送的资讯
	private String infoname;		// 推荐资讯名称
	private String department;		// 资讯发送部门
	private String detailurl;		// 跳转路径
	private String img;		// 图片0
	private String img1;		// 图片1
	private String img2;		// 图片2
	private String ico;		// ico
	private String videourl;		// 视频路径
	private Integer isrecommend;		// 是否推荐 1表示推荐 0表示不推荐
	private String senderid;		// 发送者id
	private String sendername;		// 发送者姓名
	private String extend1;		// extend1
	private String extend2;		// extend2
	private String extend3;		// extend3
	
	private String collectcount;   //收藏次数
	private String iscollect;   //是否收藏
	
	
	public Tinformation() {
		this(null);
	}

	public Tinformation(String id){
		super(id);
	}
	
	
	
	public String getIscollect() {
		return iscollect;
	}

	public void setIscollect(String iscollect) {
		this.iscollect = iscollect;
	}

	public String getCollectcount() {
		return collectcount;
	}

	public void setCollectcount(String collectcount) {
		this.collectcount = collectcount;
	}

	public String getInfoid() {
		return infoid;
	}

	public void setInfoid(String infoid) {
		this.infoid = infoid;
	}
	
	@Length(min=0, max=64, message="推荐资讯名称长度不能超过 64 个字符")
	public String getInfoname() {
		return infoname;
	}

	public void setInfoname(String infoname) {
		this.infoname = infoname;
	}
	
	@Length(min=0, max=64, message="资讯发送部门长度不能超过 64 个字符")
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	
	@Length(min=0, max=1024, message="跳转路径长度不能超过 1024 个字符")
	public String getDetailurl() {
		return detailurl;
	}

	public void setDetailurl(String detailurl) {
		this.detailurl = detailurl;
	}
	
	@Length(min=0, max=255, message="图片0长度不能超过 255 个字符")
	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}
	
	@Length(min=0, max=255, message="图片1长度不能超过 255 个字符")
	public String getImg1() {
		return img1;
	}

	public void setImg1(String img1) {
		this.img1 = img1;
	}
	
	@Length(min=0, max=255, message="图片2长度不能超过 255 个字符")
	public String getImg2() {
		return img2;
	}

	public void setImg2(String img2) {
		this.img2 = img2;
	}
	
	@Length(min=0, max=255, message="ico长度不能超过 255 个字符")
	public String getIco() {
		return ico;
	}

	public void setIco(String ico) {
		this.ico = ico;
	}
	
	@Length(min=0, max=1024, message="视频路径长度不能超过 1024 个字符")
	public String getVideourl() {
		return videourl;
	}

	public void setVideourl(String videourl) {
		this.videourl = videourl;
	}
	
	public Integer getIsrecommend() {
		return isrecommend;
	}

	public void setIsrecommend(Integer isrecommend) {
		this.isrecommend = isrecommend;
	}
	
	@Length(min=0, max=20, message="发送者id长度不能超过 20 个字符")
	public String getSenderid() {
		return senderid;
	}

	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}
	
	@Length(min=0, max=60, message="发送者姓名长度不能超过 60 个字符")
	public String getSendername() {
		return sendername;
	}

	public void setSendername(String sendername) {
		this.sendername = sendername;
	}
	
	@Length(min=0, max=255, message="extend1长度不能超过 255 个字符")
	public String getExtend1() {
		return extend1;
	}

	public void setExtend1(String extend1) {
		this.extend1 = extend1;
	}
	
	@Length(min=0, max=255, message="extend2长度不能超过 255 个字符")
	public String getExtend2() {
		return extend2;
	}

	public void setExtend2(String extend2) {
		this.extend2 = extend2;
	}
	
	@Length(min=0, max=255, message="extend3长度不能超过 255 个字符")
	public String getExtend3() {
		return extend3;
	}

	public void setExtend3(String extend3) {
		this.extend3 = extend3;
	}
	
	public Date getCreateDate_gte() {
		return sqlMap.getWhere().getValue("create_date", QueryType.GTE);
	}

	public void setCreateDate_gte(Date createDate) {
		sqlMap.getWhere().and("create_date", QueryType.GTE, createDate);
	}
	
	public Date getCreateDate_lte() {
		return sqlMap.getWhere().getValue("create_date", QueryType.LTE);
	}

	public void setCreateDate_lte(Date createDate) {
		sqlMap.getWhere().and("create_date", QueryType.LTE, createDate);
	}
	
}