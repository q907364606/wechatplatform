/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_wxmember.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.t_wxmember.entity.Twxmember;
import com.jeesite.modules.t_wxmember.dao.TwxmemberDao;
import com.jeesite.modules.file.utils.FileUploadUtils;

/**
 * 微信用户Service
 * @author zwz
 * @version 2018-10-22
 */
@Service
@Transactional(readOnly=true)
public class TwxmemberService extends CrudService<TwxmemberDao, Twxmember> {
	
	/**
	 * 获取单条数据
	 * @param twxmember
	 * @return
	 */
	@Override
	public Twxmember get(Twxmember twxmember) {
		return super.get(twxmember);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param twxmember
	 * @return
	 */
	@Override
	public Page<Twxmember> findPage(Page<Twxmember> page, Twxmember twxmember) {
		return super.findPage(page, twxmember);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param twxmember
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Twxmember twxmember) {
		super.save(twxmember);
		// 保存上传图片
		FileUploadUtils.saveFileUpload(twxmember.getId(), "twxmember_image");
		// 保存上传附件
		FileUploadUtils.saveFileUpload(twxmember.getId(), "twxmember_file");
	}
	
	/**
	 * 更新状态
	 * @param twxmember
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Twxmember twxmember) {
		super.updateStatus(twxmember);
	}
	
	/**
	 * 删除数据
	 * @param twxmember
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Twxmember twxmember) {
		super.delete(twxmember);
	}
	
}