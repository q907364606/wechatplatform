/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_wxmember.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.lang.StringUtils;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.sys.entity.EmpUser;
import com.jeesite.modules.sys.service.EmpUserService;
import com.jeesite.modules.t_wxmember.entity.Twxmember;
import com.jeesite.modules.t_wxmember.service.TwxmemberService;

/**
 * 微信用户Controller
 * @author zwz
 * @version 2018-10-18
 * 
 */
@Controller
@RequestMapping(value = "${adminPath}/t_wxmember/twxmember")
public class TwxmemberController extends BaseController {

	@Autowired
	private EmpUserService empUserService;
	
	@Autowired
	private TwxmemberService twxmemberService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Twxmember get(String memberid, boolean isNewRecord) {
		return twxmemberService.get(memberid, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("t_wxmember:twxmember:view")
	@RequestMapping(value = {"list", ""})
	public String list(Twxmember twxmember, Model model) {
		model.addAttribute("twxmember", twxmember);
		return "modules/t_wxmember/twxmemberList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("t_wxmember:twxmember:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Twxmember> listData(Twxmember twxmember, HttpServletRequest request, HttpServletResponse response) {
		Page<Twxmember> page = twxmemberService.findPage(new Page<Twxmember>(request, response), twxmember); 
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("t_wxmember:twxmember:view")
	@RequestMapping(value = "form")
	public String form(Twxmember twxmember, Model model) {
		model.addAttribute("twxmember", twxmember);
		return "modules/t_wxmember/twxmemberForm";
	}

	/**
	 * 保存微信用户(模块)
	 */
	@RequiresPermissions("t_wxmember:twxmember:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Twxmember twxmember) {
		twxmemberService.save(twxmember);
		return renderResult(Global.TRUE, text("保存微信用户(模块)成功！"));
	}
	

	
	/**
	 * 更改用户设置
	 */
	//@RequiresPermissions("t_wxmember:twxmember:edit")
	@PostMapping(value = "setUserSetting")
	@ResponseBody
	public Twxmember /*String*/ setUserSetting(@Validated Twxmember twxmember) {
		
		if( StringUtils.isBlank( twxmember.getDepartment() ) ){
			twxmember.setDepartment( null );
		}
		if( StringUtils.isBlank( twxmember.getUsername() ) ){
			twxmember.setUsername( null );
		}
		if( StringUtils.isBlank( twxmember.getPhone() ) ){
			twxmember.setPhone( null );
		}
		if( StringUtils.isBlank( twxmember.getCompanyemail() ) ){
			twxmember.setCompanyemail( null );
		}
		if( StringUtils.isBlank( twxmember.getWorklocation(  )) ){
			twxmember.setWorklocation( null );
		}
		
		
//		twxmemberService.save(twxmember);
		Twxmember findtwxmember = new Twxmember();
		findtwxmember.setOpenid( twxmember.getOpenid() );
		findtwxmember = twxmemberService.findList( findtwxmember ).get(0);
		twxmember.setMemberid( findtwxmember.getMemberid() );
		twxmemberService.update(twxmember);
		return twxmemberService.get( findtwxmember.getId() );
		
//		return renderResult(Global.TRUE, text("设置成功！"));
		
		
	}
	
	

/**
	 * 微信登录模块
	 */
//	@RequiresPermissions("t_wxmember:twxmember:edit")
	@PostMapping(value = "wxlogin")
	@ResponseBody
	public Twxmember wxlogin(@Validated Twxmember twxmember) {
		
		//根据用户传来的 openid到系统中查找是否有对应用户
		Twxmember wxmember = new Twxmember();
		wxmember.setOpenid(twxmember.getOpenid());
		//1.有则 查询 t_wxmember这张表  把  变成登陆状态返回用户
		if( twxmemberService.findList(wxmember).size()>0 ){
			
			Twxmember wxmember1 = new Twxmember();
			wxmember1.setLoginstatus("1");
			twxmemberService.update(wxmember1);
			twxmemberService.findList(wxmember).get(0).setPassword("看不到看不到");
			return twxmemberService.findList(wxmember).get(0);
		}
		//2.没有则向数据库当中新添加一个用户,把登陆状态变成1,然后返回用户
		// 无效   2.没有则把用户  则向 js_sys_user 这张表中增加一个用户，然后 查询 t_wxmember这张表  把  变成登陆状态返回用户，返回 t_wxmember 中的用户
		else{
			
			/*
			//向 js_sys_user 这张表中增加一个用户
			EmpUser empUser = new EmpUser();
			empUser.setWxOpenid( twxmember.getOpenid() );
			empUser.setLoginCode( twxmember.getOpenid() );
			empUser.setUserName( twxmember.getOpenid() );
			empUserService.save( empUser );
			*/
			
			
			twxmember.setLoginstatus("1");
			twxmemberService.save(twxmember);
			twxmemberService.findList(wxmember).get(0).setPassword("看不到看不到");
			return twxmemberService.findList(twxmember).get(0);
		}
		
	}
	
	
	/**
	 * 停用微信用户(模块)
	 */
	@RequiresPermissions("t_wxmember:twxmember:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(Twxmember twxmember) {
		twxmember.setStatus(Twxmember.STATUS_DISABLE);
		twxmemberService.updateStatus(twxmember);
		return renderResult(Global.TRUE, text("停用微信用户(模块)成功"));
	}
	
	/**
	 * 启用微信用户(模块)
	 */
	@RequiresPermissions("t_wxmember:twxmember:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(Twxmember twxmember) {
		twxmember.setStatus(Twxmember.STATUS_NORMAL);
		twxmemberService.updateStatus(twxmember);
		return renderResult(Global.TRUE, text("启用微信用户(模块)成功"));
	}
	
	/**
	 * 删除微信用户(模块)
	 */
	@RequiresPermissions("t_wxmember:twxmember:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Twxmember twxmember) {
		twxmemberService.delete(twxmember);
		return renderResult(Global.TRUE, text("删除微信用户(模块)成功！"));
	}
	
}