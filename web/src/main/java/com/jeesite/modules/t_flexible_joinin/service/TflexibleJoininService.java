/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_flexible_joinin.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.t_flexible_joinin.entity.TflexibleJoinin;
import com.jeesite.modules.t_flexible_joinin.dao.TflexibleJoininDao;
import com.jeesite.modules.file.utils.FileUploadUtils;

/**
 * 我参与的活动Service
 * @author zwz
 * @version 2018-10-16
 */
@Service
@Transactional(readOnly=true)
public class TflexibleJoininService extends CrudService<TflexibleJoininDao, TflexibleJoinin> {
	
	/**
	 * 获取单条数据
	 * @param tflexibleJoinin
	 * @return
	 */
	@Override
	public TflexibleJoinin get(TflexibleJoinin tflexibleJoinin) {
		return super.get(tflexibleJoinin);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param tflexibleJoinin
	 * @return
	 */
	@Override
	public Page<TflexibleJoinin> findPage(Page<TflexibleJoinin> page, TflexibleJoinin tflexibleJoinin) {
		return super.findPage(page, tflexibleJoinin);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param tflexibleJoinin
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(TflexibleJoinin tflexibleJoinin) {
		super.save(tflexibleJoinin);
		// 保存上传图片
		FileUploadUtils.saveFileUpload(tflexibleJoinin.getId(), "tflexibleJoinin_image");
		// 保存上传附件
		FileUploadUtils.saveFileUpload(tflexibleJoinin.getId(), "tflexibleJoinin_file");
	}
	
	/**
	 * 更新状态
	 * @param tflexibleJoinin
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(TflexibleJoinin tflexibleJoinin) {
		super.updateStatus(tflexibleJoinin);
	}
	
	/**
	 * 删除数据
	 * @param tflexibleJoinin
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(TflexibleJoinin tflexibleJoinin) {
		super.delete(tflexibleJoinin);
	}
	
}