/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_flexible_joinin.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.t_flexible_joinin.entity.TflexibleJoinin;

/**
 * 我参与的活动DAO接口
 * @author zwz
 * @version 2018-10-16
 */
@MyBatisDao
public interface TflexibleJoininDao extends CrudDao<TflexibleJoinin> {
	
}