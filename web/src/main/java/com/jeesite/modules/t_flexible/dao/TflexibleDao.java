/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_flexible.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.t_flexible.entity.Tflexible;

/**
 * 发起的活动DAO接口
 * @author zwz
 * @version 2018-10-20
 */
@MyBatisDao
public interface TflexibleDao extends CrudDao<Tflexible> {
	
}