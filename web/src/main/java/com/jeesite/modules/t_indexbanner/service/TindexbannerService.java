/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_indexbanner.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.t_indexbanner.entity.Tindexbanner;
import com.jeesite.modules.t_indexbanner.dao.TindexbannerDao;
import com.jeesite.modules.file.utils.FileUploadUtils;

/**
 * 首页bannerService
 * @author zwz
 * @version 2018-10-12
 */
@Service
@Transactional(readOnly=true)
public class TindexbannerService extends CrudService<TindexbannerDao, Tindexbanner> {
	
	/**
	 * 获取单条数据
	 * @param tindexbanner
	 * @return
	 */
	@Override
	public Tindexbanner get(Tindexbanner tindexbanner) {
		return super.get(tindexbanner);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param tindexbanner
	 * @return
	 */
	@Override
	public Page<Tindexbanner> findPage(Page<Tindexbanner> page, Tindexbanner tindexbanner) {
		return super.findPage(page, tindexbanner);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param tindexbanner
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Tindexbanner tindexbanner) {
		super.save(tindexbanner);
		// 保存上传图片
		FileUploadUtils.saveFileUpload(tindexbanner.getId(), "tindexbanner_image");
		// 保存上传附件
		FileUploadUtils.saveFileUpload(tindexbanner.getId(), "tindexbanner_file");
	}
	
	/**
	 * 更新状态
	 * @param tindexbanner
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Tindexbanner tindexbanner) {
		super.updateStatus(tindexbanner);
	}
	
	/**
	 * 删除数据
	 * @param tindexbanner
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Tindexbanner tindexbanner) {
		super.delete(tindexbanner);
	}
	
}