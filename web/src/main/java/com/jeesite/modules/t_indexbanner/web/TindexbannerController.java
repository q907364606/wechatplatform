/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_indexbanner.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.t_indexbanner.entity.Tindexbanner;
import com.jeesite.modules.t_indexbanner.service.TindexbannerService;

/**
 * 首页bannerController
 * @author zwz
 * @version 2018-10-12
 */
@Controller
@RequestMapping(value = "${adminPath}/t_indexbanner/tindexbanner")
public class TindexbannerController extends BaseController {

	@Autowired
	private TindexbannerService tindexbannerService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Tindexbanner get(String bannerid, boolean isNewRecord) {
		return tindexbannerService.get(bannerid, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("t_indexbanner:tindexbanner:view")
	@RequestMapping(value = {"list", ""})
	public String list(Tindexbanner tindexbanner, Model model) {
		model.addAttribute("tindexbanner", tindexbanner);
		return "modules/t_indexbanner/tindexbannerList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("t_indexbanner:tindexbanner:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Tindexbanner> listData(Tindexbanner tindexbanner, HttpServletRequest request, HttpServletResponse response) {
		Page<Tindexbanner> page = tindexbannerService.findPage(new Page<Tindexbanner>(request, response), tindexbanner); 
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("t_indexbanner:tindexbanner:view")
	@RequestMapping(value = "form")
	public String form(Tindexbanner tindexbanner, Model model) {
		model.addAttribute("tindexbanner", tindexbanner);
		return "modules/t_indexbanner/tindexbannerForm";
	}

	/**
	 * 保存indexbanner
	 */
	@RequiresPermissions("t_indexbanner:tindexbanner:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Tindexbanner tindexbanner) {
		tindexbannerService.save(tindexbanner);
		return renderResult(Global.TRUE, text("保存indexbanner成功！"));
	}
	
	/**
	 * 停用indexbanner
	 */
	@RequiresPermissions("t_indexbanner:tindexbanner:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(Tindexbanner tindexbanner) {
		tindexbanner.setStatus(Tindexbanner.STATUS_DISABLE);
		tindexbannerService.updateStatus(tindexbanner);
		return renderResult(Global.TRUE, text("停用indexbanner成功"));
	}
	
	/**
	 * 启用indexbanner
	 */
	@RequiresPermissions("t_indexbanner:tindexbanner:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(Tindexbanner tindexbanner) {
		tindexbanner.setStatus(Tindexbanner.STATUS_NORMAL);
		tindexbannerService.updateStatus(tindexbanner);
		return renderResult(Global.TRUE, text("启用indexbanner成功"));
	}
	
	/**
	 * 删除indexbanner
	 */
	@RequiresPermissions("t_indexbanner:tindexbanner:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Tindexbanner tindexbanner) {
		tindexbannerService.delete(tindexbanner);
		return renderResult(Global.TRUE, text("删除indexbanner成功！"));
	}
	
}