/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_flexible2.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.t_flexible2.entity.TflexibleJoinin2;

/**
 * 发起的活动DAO接口
 * @author zwz
 * @version 2018-10-20
 */
@MyBatisDao
public interface TflexibleJoinin2Dao extends CrudDao<TflexibleJoinin2> {
	
}