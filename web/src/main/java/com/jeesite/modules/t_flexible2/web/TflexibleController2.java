/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_flexible2.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.t_flexible2.entity.Tflexible2;
import com.jeesite.modules.t_flexible2.service.TflexibleService2;

/**
 * 发起的活动Controller
 * @author zwz
 * @version 2018-10-20
 */
@Controller
@RequestMapping(value = "${adminPath}/t_flexible2/tflexible")
public class TflexibleController2 extends BaseController {

	@Autowired
	private TflexibleService2 tflexibleService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public Tflexible2 get(String flexibleid, boolean isNewRecord) {
		return tflexibleService.get(flexibleid, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("t_flexible2:tflexible:view")
	@RequestMapping(value = {"list", ""})
	public String list(Tflexible2 tflexible, Model model) {
		model.addAttribute("tflexible", tflexible);
		return "modules/t_flexible2/tflexibleList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("t_flexible2:tflexible:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<Tflexible2> listData(Tflexible2 tflexible, HttpServletRequest request, HttpServletResponse response) {
		Page<Tflexible2> page = tflexibleService.findPage(new Page<Tflexible2>(request, response), tflexible); 
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("t_flexible2:tflexible:view")
	@RequestMapping(value = "form")
	public String form(Tflexible2 tflexible, Model model) {
		model.addAttribute("tflexible", tflexible);
		return "modules/t_flexible2/tflexibleForm";
	}

	/**
	 * 保存发起的活动
	 */
	@RequiresPermissions("t_flexible2:tflexible:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated Tflexible2 tflexible) {
		tflexibleService.save(tflexible);
		return renderResult(Global.TRUE, text("保存发起的活动成功！"));
	}
	
	/**
	 * 停用发起的活动
	 */
	@RequiresPermissions("t_flexible2:tflexible:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(Tflexible2 tflexible) {
		tflexible.setStatus(Tflexible2.STATUS_DISABLE);
		tflexibleService.updateStatus(tflexible);
		return renderResult(Global.TRUE, text("停用发起的活动成功"));
	}
	
	/**
	 * 启用发起的活动
	 */
	@RequiresPermissions("t_flexible2:tflexible:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(Tflexible2 tflexible) {
		tflexible.setStatus(Tflexible2.STATUS_NORMAL);
		tflexibleService.updateStatus(tflexible);
		return renderResult(Global.TRUE, text("启用发起的活动成功"));
	}
	
	/**
	 * 删除发起的活动
	 */
	@RequiresPermissions("t_flexible2:tflexible:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(Tflexible2 tflexible) {
		tflexibleService.delete(tflexible);
		return renderResult(Global.TRUE, text("删除发起的活动成功！"));
	}
	
}