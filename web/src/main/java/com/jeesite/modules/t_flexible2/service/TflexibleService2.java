/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_flexible2.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.t_flexible2.entity.Tflexible2;
import com.jeesite.modules.t_flexible2.dao.TflexibleDao2;
import com.jeesite.modules.file.utils.FileUploadUtils;
import com.jeesite.modules.t_flexible2.entity.TflexibleJoinin2;
import com.jeesite.modules.t_flexible2.dao.TflexibleJoinin2Dao;

/**
 * 发起的活动Service
 * @author zwz
 * @version 2018-10-20
 */
@Service
@Transactional(readOnly=true)
public class TflexibleService2 extends CrudService<TflexibleDao2, Tflexible2> {
	
	@Autowired
	private TflexibleJoinin2Dao tflexibleJoinin2Dao;
	
	/**
	 * 获取单条数据
	 * @param tflexible
	 * @return
	 */
	@Override
	public Tflexible2 get(Tflexible2 tflexible) {
		Tflexible2 entity = super.get(tflexible);
		if (entity != null){
			TflexibleJoinin2 tflexibleJoinin2 = new TflexibleJoinin2(entity);
			tflexibleJoinin2.setStatus(TflexibleJoinin2.STATUS_NORMAL);
			entity.setTflexibleJoinin2List(tflexibleJoinin2Dao.findList(tflexibleJoinin2));
		}
		return entity;
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param tflexible
	 * @return
	 */
	@Override
	public Page<Tflexible2> findPage(Page<Tflexible2> page, Tflexible2 tflexible) {
		return super.findPage(page, tflexible);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param tflexible
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(Tflexible2 tflexible) {
		super.save(tflexible);
		// 保存上传图片
		FileUploadUtils.saveFileUpload(tflexible.getId(), "tflexible_image");
		// 保存上传附件
		FileUploadUtils.saveFileUpload(tflexible.getId(), "tflexible_file");
		// 保存 Tflexible子表
		for (TflexibleJoinin2 tflexibleJoinin2 : tflexible.getTflexibleJoinin2List()){
			if (!TflexibleJoinin2.STATUS_DELETE.equals(tflexibleJoinin2.getStatus())){
				tflexibleJoinin2.setFlexibleid(tflexible);
				if (tflexibleJoinin2.getIsNewRecord()){
					tflexibleJoinin2.preInsert();
					tflexibleJoinin2Dao.insert(tflexibleJoinin2);
				}else{
					tflexibleJoinin2.preUpdate();
					tflexibleJoinin2Dao.update(tflexibleJoinin2);
				}
			}else{
				tflexibleJoinin2Dao.delete(tflexibleJoinin2);
			}
		}
	}
	
	/**
	 * 更新状态
	 * @param tflexible
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(Tflexible2 tflexible) {
		super.updateStatus(tflexible);
	}
	
	/**
	 * 删除数据
	 * @param tflexible
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(Tflexible2 tflexible) {
		super.delete(tflexible);
		TflexibleJoinin2 tflexibleJoinin2 = new TflexibleJoinin2();
		tflexibleJoinin2.setFlexibleid(tflexible);
		tflexibleJoinin2Dao.delete(tflexibleJoinin2);
	}
	
}