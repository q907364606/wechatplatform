/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_collect_information.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.t_collect_information.entity.TcollectInformation;
import com.jeesite.modules.t_collect_information.dao.TcollectInformationDao;
import com.jeesite.modules.file.utils.FileUploadUtils;

/**
 * 我收藏的资讯Service
 * @author zwz
 * @version 2018-10-19
 */
@Service
@Transactional(readOnly=true)
public class TcollectInformationService extends CrudService<TcollectInformationDao, TcollectInformation> {
	
	/**
	 * 获取单条数据
	 * @param tcollectInformation
	 * @return
	 */
	@Override
	public TcollectInformation get(TcollectInformation tcollectInformation) {
		return super.get(tcollectInformation);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param tcollectInformation
	 * @return
	 */
	@Override
	public Page<TcollectInformation> findPage(Page<TcollectInformation> page, TcollectInformation tcollectInformation) {
		return super.findPage(page, tcollectInformation);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param tcollectInformation
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(TcollectInformation tcollectInformation) {
		super.save(tcollectInformation);
		// 保存上传图片
		FileUploadUtils.saveFileUpload(tcollectInformation.getId(), "tcollectInformation_image");
		// 保存上传附件
		FileUploadUtils.saveFileUpload(tcollectInformation.getId(), "tcollectInformation_file");
	}
	
	/**
	 * 更新状态
	 * @param tcollectInformation
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(TcollectInformation tcollectInformation) {
		super.updateStatus(tcollectInformation);
	}
	
	/**
	 * 删除数据
	 * @param tcollectInformation
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(TcollectInformation tcollectInformation) {
		super.delete(tcollectInformation);
	}
	
}