/**
   * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_collect_information.web;

import java.lang.reflect.InvocationTargetException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.t_collect_information.entity.TcollectInformation;
import com.jeesite.modules.t_collect_information.service.TcollectInformationService;
import com.jeesite.modules.t_information.entity.Tinformation;
import com.jeesite.modules.t_information.service.TinformationService;
import com.jeesite.modules.t_wxmember.entity.Twxmember;
import com.jeesite.modules.t_wxmember.service.TwxmemberService;

/**
 * 我收藏的资讯Controller
 * @author zwz
 * @version 2018-10-19
 */
@Controller
@RequestMapping(value = "${adminPath}/t_collect_information/tcollectInformation")
public class TcollectInformationController extends BaseController {

	@Autowired
	private TcollectInformationService tcollectInformationService;
	
	@Autowired
	private TwxmemberService twxmemberService;
	
	@Autowired
	private TinformationService tinformationService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public TcollectInformation get(String collectionid, boolean isNewRecord) {
		return tcollectInformationService.get(collectionid, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	//@RequiresPermissions("t_collect_information:tcollectInformation:view")
	@RequestMapping(value = {"list", ""})
	public String list(TcollectInformation tcollectInformation, Model model) {
		model.addAttribute("tcollectInformation", tcollectInformation);
		return "modules/t_collect_information/tcollectInformationList";
	}
	
	/**
	 * 查询列表数据
	 */
	//@RequiresPermissions("t_collect_information:tcollectInformation:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<TcollectInformation> listData(TcollectInformation tcollectInformation, HttpServletRequest request, HttpServletResponse response) {
		
		Page<TcollectInformation> pg = new Page<TcollectInformation>(request, response);
		pg.setPageNo(  Integer.parseInt(request.getParameter("pageNo"))  );
		pg.setPageSize(  Integer.parseInt( request.getParameter("pageSize") )  );
		
//		Page<TcollectInformation> page = tcollectInformationService.findPage(new Page<TcollectInformation>(request, response), tcollectInformation); 
		Page<TcollectInformation> page = tcollectInformationService.findPage( pg , tcollectInformation); 
		return page;
	
	}

	/**
	 * 查看编辑表单
	 */
	//@RequiresPermissions("t_collect_information:tcollectInformation:view")
	@RequestMapping(value = "form")
	public String form(TcollectInformation tcollectInformation, Model model) {
		model.addAttribute("tcollectInformation", tcollectInformation);
		return "modules/t_collect_information/tcollectInformationForm";
	}

	/**
	 * 保存我收藏的资讯
	 */
	//@RequiresPermissions("t_collect_information:tcollectInformation:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated TcollectInformation tcollectInformation) {
		tcollectInformationService.save(tcollectInformation);
		return renderResult(Global.TRUE, text("保存我收藏的资讯成功！"));
	}
	
	
	
	/**
	 * 点击收藏按钮
	 * @param tcollectInformation
	 * @return
	 */
//	@PostMapping(value = "clickcollect")
	@RequestMapping(value="clickcollect")
	@ResponseBody
	public String clickcollect(@Validated TcollectInformation tcollectInformation) {
		
		//根据openid查询出用户id
		Twxmember twxmember = new Twxmember();
		twxmember.setOpenid( tcollectInformation.getOpenid() );
		twxmember = twxmemberService.findList( twxmember ).get(0);
		
		//查询出讯息表中的内容
		Tinformation tinformation = tinformationService.get( tcollectInformation.getInfoid() );
		
		//先判断是否有收藏过这条消息
		TcollectInformation tci = new TcollectInformation();
		tci.setInfoid( tcollectInformation.getInfoid() );
		if( tcollectInformationService.findList( tci ).size()>0 ){
			return renderResult(Global.TRUE, text("重复收藏消息！"));	
		}else{
		
			//设置用户的id
			tcollectInformation.setMemberid( twxmember.getMemberid() );
			tcollectInformation.setInfoname( tinformation.getInfoname() );
			tcollectInformation.setDepartment( tinformation.getDepartment() );
			tcollectInformation.setSendername( tinformation.getSendername() );
			tcollectInformation.setSenderid( tinformation.getSenderid() );
			tcollectInformation.setDetailurl( tinformation.getDetailurl() );
			tcollectInformation.setImg(  tinformation.getImg() );
			tcollectInformation.setImg1( tinformation.getImg1()  );
			tcollectInformation.setImg2(  tinformation.getImg2() );
			tcollectInformation.setIco(  tinformation.getIco()  );
			tcollectInformation.setExtend1(  tinformation.getExtend1() );
			tcollectInformation.setExtend2(  tinformation.getExtend2() );
			tcollectInformation.setExtend3(  tinformation.getExtend3() );
			tcollectInformation.setVideourl( tinformation.getVideourl()  );
			tcollectInformation.setStatus(  tinformation.getStatus()  );
			tcollectInformation.setRemarks(  tinformation.getRemarks() );
			tcollectInformation.setIsrecommend(  tinformation.getIsrecommend()  );
			
	//		BeanUtils.copyProperties( tinformation, tcollectInformation);
	//		tcollectInformation.setCollectionid(null);
			//保存用户讯息
			tcollectInformationService.save(tcollectInformation);
			return renderResult(Global.TRUE, text("保存我收藏的资讯成功！"));
		}
	}
	
	
	
	/**
	 * 停用我收藏的资讯
	 */
	//@RequiresPermissions("t_collect_information:tcollectInformation:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(TcollectInformation tcollectInformation) {
		tcollectInformation.setStatus(TcollectInformation.STATUS_DISABLE);
		tcollectInformationService.updateStatus(tcollectInformation);
		return renderResult(Global.TRUE, text("停用我收藏的资讯成功"));
	}
	
	/**
	 * 启用我收藏的资讯
	 */
	//@RequiresPermissions("t_collect_information:tcollectInformation:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(TcollectInformation tcollectInformation) {
		tcollectInformation.setStatus(TcollectInformation.STATUS_NORMAL);
		tcollectInformationService.updateStatus(tcollectInformation);
		return renderResult(Global.TRUE, text("启用我收藏的资讯成功"));
	}
	
	/**
	 * 删除我收藏的资讯
	 */
	//@RequiresPermissions("t_collect_information:tcollectInformation:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(TcollectInformation tcollectInformation) {
		tcollectInformationService.delete(tcollectInformation);
		return renderResult(Global.TRUE, text("删除我收藏的资讯成功！"));
	}
	
}