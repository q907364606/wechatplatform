/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_collect_information.entity;

import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 我收藏的资讯Entity
 * @author zwz
 * @version 2018-10-19
 */
@Table(name="t_collect_information", alias="a", columns={
		@Column(name="collectionid", attrName="collectionid", label="我收藏的资讯id（是一个主键）  加入到这张表中的数据都是用户点击收集的资讯  ", comment="我收藏的资讯id（是一个主键）  加入到这张表中的数据都是用户点击收集的资讯  (收藏的资讯表是 t_information)", isPK=true),
		@Column(name="memberid", attrName="memberid", label="用户的id 关联 t_wxmember 中的主键"),
		@Column(name="openid", attrName="openid", label="微信openid"),
		@Column(name="infoid", attrName="infoid", label="收藏资讯的id"),
		@Column(name="infoname", attrName="infoname", label="推荐资讯的名称"),
		@Column(name="department", attrName="department", label="部门"),
		@Column(name="sendername", attrName="sendername", label="发送者名称"),
		@Column(name="senderid", attrName="senderid", label="发送者id"),
		@Column(name="detailurl", attrName="detailurl", label="跳转url"),
		@Column(name="img", attrName="img", label="图片0"),
		@Column(name="img1", attrName="img1", label="图片1"),
		@Column(name="img2", attrName="img2", label="图片2"),
		@Column(name="ico", attrName="ico", label="ico信息"),
		@Column(name="extend1", attrName="extend1", label="扩展1"),
		@Column(name="extend2", attrName="extend2", label="扩展2"),
		@Column(name="extend3", attrName="extend3", label="扩展3"),
		@Column(name="videourl", attrName="videourl", label="视频url"),
		@Column(includeEntity=DataEntity.class),
		@Column(name="isrecommend", attrName="isrecommend", label="是否推荐 1表示推荐 0表示不推荐"),
	}, orderBy="a.update_date DESC"
)
public class TcollectInformation extends DataEntity<TcollectInformation> {
	
	private static final long serialVersionUID = 1L;
	private String collectionid;		// 我收藏的资讯id（是一个主键）  加入到这张表中的数据都是用户点击收集的资讯  (收藏的资讯表是 t_information)
	private String memberid;		// 用户的id 关联 t_wxmember 中的主键
	private String openid;		// 微信openid
	private String infoid;		// 收藏资讯的id
	private String infoname;		// 推荐资讯的名称
	private String department;		// 部门
	private String sendername;		// 发送者名称
	private String senderid;		// 发送者id
	private String detailurl;		// 跳转url
	private String img;		// 图片0
	private String img1;		// 图片1
	private String img2;		// 图片2
	private String ico;		// ico信息
	private String extend1;		// 扩展1
	private String extend2;		// 扩展2
	private String extend3;		// 扩展3
	private String videourl;		// 视频url
	private Integer isrecommend;		// 是否推荐 1表示推荐 0表示不推荐
	
	public TcollectInformation() {
		this(null);
	}

	public TcollectInformation(String id){
		super(id);
	}
	
	public String getCollectionid() {
		return collectionid;
	}

	public void setCollectionid(String collectionid) {
		this.collectionid = collectionid;
	}
	
	@Length(min=0, max=20, message="用户的id 关联 t_wxmember 中的主键长度不能超过 20 个字符")
	public String getMemberid() {
		return memberid;
	}

	public void setMemberid(String memberid) {
		this.memberid = memberid;
	}
	
	@Length(min=0, max=64, message="微信openid长度不能超过 64 个字符")
	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}
	
	@Length(min=0, max=64, message="收藏资讯的id长度不能超过 64 个字符")
	public String getInfoid() {
		return infoid;
	}

	public void setInfoid(String infoid) {
		this.infoid = infoid;
	}
	
	@Length(min=0, max=64, message="推荐资讯的名称长度不能超过 64 个字符")
	public String getInfoname() {
		return infoname;
	}

	public void setInfoname(String infoname) {
		this.infoname = infoname;
	}
	
	@Length(min=0, max=64, message="部门长度不能超过 64 个字符")
	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}
	
	@Length(min=0, max=64, message="发送者名称长度不能超过 64 个字符")
	public String getSendername() {
		return sendername;
	}

	public void setSendername(String sendername) {
		this.sendername = sendername;
	}
	
	@Length(min=0, max=64, message="发送者id长度不能超过 64 个字符")
	public String getSenderid() {
		return senderid;
	}

	public void setSenderid(String senderid) {
		this.senderid = senderid;
	}
	
	@Length(min=0, max=1024, message="跳转url长度不能超过 1024 个字符")
	public String getDetailurl() {
		return detailurl;
	}

	public void setDetailurl(String detailurl) {
		this.detailurl = detailurl;
	}
	
	@Length(min=0, max=1024, message="图片0长度不能超过 1024 个字符")
	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}
	
	@Length(min=0, max=1024, message="图片1长度不能超过 1024 个字符")
	public String getImg1() {
		return img1;
	}

	public void setImg1(String img1) {
		this.img1 = img1;
	}
	
	@Length(min=0, max=1024, message="图片2长度不能超过 1024 个字符")
	public String getImg2() {
		return img2;
	}

	public void setImg2(String img2) {
		this.img2 = img2;
	}
	
	@Length(min=0, max=1024, message="ico信息长度不能超过 1024 个字符")
	public String getIco() {
		return ico;
	}

	public void setIco(String ico) {
		this.ico = ico;
	}
	
	@Length(min=0, max=255, message="扩展1长度不能超过 255 个字符")
	public String getExtend1() {
		return extend1;
	}

	public void setExtend1(String extend1) {
		this.extend1 = extend1;
	}
	
	@Length(min=0, max=255, message="扩展2长度不能超过 255 个字符")
	public String getExtend2() {
		return extend2;
	}

	public void setExtend2(String extend2) {
		this.extend2 = extend2;
	}
	
	@Length(min=0, max=255, message="扩展3长度不能超过 255 个字符")
	public String getExtend3() {
		return extend3;
	}

	public void setExtend3(String extend3) {
		this.extend3 = extend3;
	}
	
	@Length(min=0, max=1024, message="视频url长度不能超过 1024 个字符")
	public String getVideourl() {
		return videourl;
	}

	public void setVideourl(String videourl) {
		this.videourl = videourl;
	}
	
	public Integer getIsrecommend() {
		return isrecommend;
	}

	public void setIsrecommend(Integer isrecommend) {
		this.isrecommend = isrecommend;
	}
	
}