/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_csii_headline.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.t_csii_headline.entity.TcsiiHeadline;

/**
 * 科蓝头条DAO接口
 * @author zwz
 * @version 2018-10-23
 */
@MyBatisDao
public interface TcsiiHeadlineDao extends CrudDao<TcsiiHeadline> {
	
}