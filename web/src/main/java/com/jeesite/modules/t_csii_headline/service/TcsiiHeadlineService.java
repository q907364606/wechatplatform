/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_csii_headline.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.t_csii_headline.entity.TcsiiHeadline;
import com.jeesite.modules.t_csii_headline.dao.TcsiiHeadlineDao;
import com.jeesite.modules.file.utils.FileUploadUtils;

/**
 * 科蓝头条Service
 * @author zwz
 * @version 2018-10-23
 */
@Service
@Transactional(readOnly=true)
public class TcsiiHeadlineService extends CrudService<TcsiiHeadlineDao, TcsiiHeadline> {
	
	/**
	 * 获取单条数据
	 * @param tcsiiHeadline
	 * @return
	 */
	@Override
	public TcsiiHeadline get(TcsiiHeadline tcsiiHeadline) {
		return super.get(tcsiiHeadline);
	}
	
	/**
	 * 查询分页数据
	 * @param page 分页对象
	 * @param tcsiiHeadline
	 * @return
	 */
	@Override
	public Page<TcsiiHeadline> findPage(Page<TcsiiHeadline> page, TcsiiHeadline tcsiiHeadline) {
		return super.findPage(page, tcsiiHeadline);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param tcsiiHeadline
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(TcsiiHeadline tcsiiHeadline) {
		super.save(tcsiiHeadline);
		// 保存上传图片
		FileUploadUtils.saveFileUpload(tcsiiHeadline.getId(), "tcsiiHeadline_image");
		// 保存上传附件
		FileUploadUtils.saveFileUpload(tcsiiHeadline.getId(), "tcsiiHeadline_file");
	}
	
	/**
	 * 更新状态
	 * @param tcsiiHeadline
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(TcsiiHeadline tcsiiHeadline) {
		super.updateStatus(tcsiiHeadline);
	}
	
	/**
	 * 删除数据
	 * @param tcsiiHeadline
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(TcsiiHeadline tcsiiHeadline) {
		super.delete(tcsiiHeadline);
	}
	
}