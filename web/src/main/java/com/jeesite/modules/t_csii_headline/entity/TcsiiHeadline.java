/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_csii_headline.entity;

import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 科蓝头条Entity
 * @author zwz
 * @version 2018-10-23
 */
@Table(name="t_csii_headline", alias="a", columns={
		@Column(name="headlineid", attrName="headlineid", label="主键id", isPK=true),
		@Column(name="title", attrName="title", label="长标题"),
		@Column(name="subtitle", attrName="subtitle", label="简短标题"),
		@Column(name="summary", attrName="summary", label="简介"),
		@Column(name="detail", attrName="detail", label="详情"),
		@Column(name="detailurl", attrName="detailurl", label="点击之后跳转的链接"),
		@Column(name="icon", attrName="icon", label="icon"),
		@Column(name="image", attrName="image", label="图片"),
		@Column(name="image1", attrName="image1", label="图片1"),
		@Column(name="image2", attrName="image2", label="图片2"),
		@Column(name="videourl", attrName="videourl", label="视频url"),
		@Column(name="extend1", attrName="extend1", label="扩展1"),
		@Column(name="extend2", attrName="extend2", label="扩展2"),
		@Column(name="extend3", attrName="extend3", label="扩展3"),
		@Column(includeEntity=DataEntity.class),
	}, orderBy="a.update_date DESC"
)
public class TcsiiHeadline extends DataEntity<TcsiiHeadline> {
	
	private static final long serialVersionUID = 1L;
	private String headlineid;		// 主键id
	private String title;		// 长标题
	private String subtitle;		// 简短标题
	private String summary;		// 简介
	private String detail;		// 详情
	private String detailurl;		// 点击之后跳转的链接
	private String icon;		// icon
	private String image;		// 图片
	private String image1;		// 图片1
	private String image2;		// 图片2
	private String videourl;		// 视频url
	private String extend1;		// 扩展1
	private String extend2;		// 扩展2
	private String extend3;		// 扩展3
	
	public TcsiiHeadline() {
		this(null);
	}

	public TcsiiHeadline(String id){
		super(id);
	}
	
	public String getHeadlineid() {
		return headlineid;
	}

	public void setHeadlineid(String headlineid) {
		this.headlineid = headlineid;
	}
	
	@Length(min=0, max=255, message="长标题长度不能超过 255 个字符")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	@Length(min=0, max=40, message="简短标题长度不能超过 40 个字符")
	public String getSubtitle() {
		return subtitle;
	}

	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	
	@Length(min=0, max=255, message="简介长度不能超过 255 个字符")
	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	@Length(min=0, max=1024, message="详情长度不能超过 1024 个字符")
	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}
	
	@Length(min=0, max=1024, message="点击之后跳转的链接长度不能超过 1024 个字符")
	public String getDetailurl() {
		return detailurl;
	}

	public void setDetailurl(String detailurl) {
		this.detailurl = detailurl;
	}
	
	@Length(min=0, max=1024, message="icon长度不能超过 1024 个字符")
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@Length(min=0, max=1024, message="图片长度不能超过 1024 个字符")
	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	@Length(min=0, max=255, message="图片1长度不能超过 255 个字符")
	public String getImage1() {
		return image1;
	}

	public void setImage1(String image1) {
		this.image1 = image1;
	}
	
	@Length(min=0, max=255, message="图片2长度不能超过 255 个字符")
	public String getImage2() {
		return image2;
	}

	public void setImage2(String image2) {
		this.image2 = image2;
	}
	
	@Length(min=0, max=255, message="视频url长度不能超过 255 个字符")
	public String getVideourl() {
		return videourl;
	}

	public void setVideourl(String videourl) {
		this.videourl = videourl;
	}
	
	@Length(min=0, max=255, message="扩展1长度不能超过 255 个字符")
	public String getExtend1() {
		return extend1;
	}

	public void setExtend1(String extend1) {
		this.extend1 = extend1;
	}
	
	@Length(min=0, max=255, message="扩展2长度不能超过 255 个字符")
	public String getExtend2() {
		return extend2;
	}

	public void setExtend2(String extend2) {
		this.extend2 = extend2;
	}
	
	@Length(min=0, max=255, message="扩展3长度不能超过 255 个字符")
	public String getExtend3() {
		return extend3;
	}

	public void setExtend3(String extend3) {
		this.extend3 = extend3;
	}
	
}