/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.t_flexible_joinin3.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.t_flexible_joinin3.entity.TflexibleJoinin3;

/**
 * 活动上传的文件DAO接口
 * @author zwz
 * @version 2018-10-21
 */
@MyBatisDao
public interface TflexibleJoininDao3 extends CrudDao<TflexibleJoinin3> {
	
}